-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 21, 2017 at 08:15 PM
-- Server version: 5.5.55-0+deb8u1
-- PHP Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `name` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `numbers` int(20) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`name`, `email`, `numbers`, `message`) VALUES
('Uma adhikari', '', 2147483647, ''),
('Uma adhikari', '', 2147483647, '');

-- --------------------------------------------------------

--
-- Table structure for table `costumer`
--

CREATE TABLE `costumer` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `age` int(11) NOT NULL,
  `address` varchar(30) DEFAULT NULL,
  `salary` decimal(20,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `costumer`
--

INSERT INTO `costumer` (`id`, `name`, `age`, `address`, `salary`) VALUES
(1, 'rahul', 34, 'ktm', 43000.0000),
(2, 'sanam', 33, 'kamalpokhari', 4300.0000),
(3, 'hari', 34, 'btm', 7600.0000),
(4, 'shyam', 55, 'sarlahi', 6700.0000),
(5, 'ramu', 12, 'bandipur', 4433.0000),
(6, 'bina', 23, 'rampur', 4533.0000),
(7, 'narayan', 33, 'gaighat', 4433.0000);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `body` text NOT NULL,
  `aurthor` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL,
  `published_date` date DEFAULT NULL,
  `published` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `body`, `aurthor`, `image`, `published_date`, `published`) VALUES
(4, 'United Nations sad over Saptari incident\r\n\r\nhttp://bit.ly/2mx0xfZ', 'Mar 7, 2017- The United Nations has expressed its sadness over the loss of life and injuries which occurred in a violent clash between cadres of Madhesi Morcha and police in Saptari district on Monday.\r\n\r\nIn a press statement on Tuesday, the UN office in Nepal said it is ‘seriously concerned by the escalation of tensions in the lead-up to the local elections announced for May’.\r\n\r\nThe UN office has urged all the parties to express their views in actions and statements instead of resorting to violence. “Violence has no place in the freedom of expression and assembly. We call on security forces to comply with existing national and international standards on the use of force,” read the statement.\r\n\r\nFurthermore, the UN in its statement has encouraged inclusive and meaningful dialogue to create environment for conducting local electi\r\n\r\n\r\nhttp://bit.ly/2mx0xfZ', 'post,report kathmandu', '', '2017-03-08', 0),
(6, 'bus', '<p>bus has been crashed</p>\r\n', 'subash basnet', '', '2012-11-11', 1),
(7, 'bus', '<p>bus chrased</p>\r\n', 'subash basnet', '', '2074-10-10', 0),
(8, 'car', '<p>lkasjdf;laksjdfa</p>\r\n', 'lambo', '', '2018-11-11', 0),
(9, 'sushma ', '<p>alksjdf;laskd</p>\r\n', 'alkdsjf;lasd', '', '2074-10-10', 0),
(10, 'sujan', '<p>kalsjdlfkasjd;lfkas21</p>\r\n', 'subash', '', '2012-11-11', 0),
(11, 'dhurghatana ma dhula ko mrityu', '<p>dhurghatna ma pari,kathford ma adhyanrit chatra bishal rau t ko mrityu.bishal afnai bibaha ma jadai garda vir bata gharera unko tei thau sthith ma bishal rau ko mrityu.</p>\r\n\r\n<p>unki hune wala srimati sushma khadka ko tei mandap ma pagal vai&nbsp; lagankhel ko mental hospital ma barti vai aaye ko xa!!!!</p>\r\n\r\n<p>hami kathford college pariwar ko sabbai bidharthi dwara shusma lai heartly condolence.</p>\r\n\r\n<p>yo sabbai news hamro priya dai sujan dwara aaye ko ho. yo ghatana kalapanik hoena kei kura milna gaye ma sabbai lai thos pugne xa....</p>\r\n\r\n<p>WRITER:SUJAN YONJAN</p>\r\n', 'sujan don', '', '2012-11-11', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `oid` int(11) NOT NULL,
  `costumer_id` int(11) NOT NULL,
  `salary` decimal(45,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`oid`, `costumer_id`, `salary`) VALUES
(0, 0, 4300.00),
(23, 1, 4300.00),
(123, 2, 3200.00),
(432, 3, 4500.00);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `price` decimal(20,2) NOT NULL,
  `Descrription` varchar(60) DEFAULT NULL,
  `Image` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `Name`, `price`, `Descrription`, `Image`) VALUES
(1, 'shirt', 1500.00, '1', ''),
(2, 'pant', 2000.00, '1', ''),
(3, 't-shirt', 1300.00, '1', ''),
(4, 'jacket', 2300.00, '1', '');

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE `register` (
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `upload_file`
--

CREATE TABLE `upload_file` (
  `documents` text NOT NULL,
  `images` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `worker`
--

CREATE TABLE `worker` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `phone` decimal(10,0) NOT NULL,
  `address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `worker`
--

INSERT INTO `worker` (`id`, `name`, `phone`, `address`) VALUES
(2, 'ram', 0, ''),
(3, 'hari', 0, 'asjdlf;kajsd;lfkas'),
(6, 'rameshor', 0, 'asjdlf;kajsd;lfkas sallaghari');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `costumer`
--
ALTER TABLE `costumer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`oid`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
