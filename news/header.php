<?php session_start(); ?>
<! DOCTYPE html>
<html>
<head>
	<title>Blog | <?php echo $title; ?></title>
	<link rel="stylesheet" type="text/css" href="/myblog/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/myblog/news/all.css">
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
  		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		    		<span class="sr-only">Toggle navigation</span>
		    		<span class="icon-bar"></span>
		    		<span class="icon-bar"></span>
		    		<span class="icon-bar"></span>
	  			</button>
				<a class="navbar-brand" href="index.php">My Blog</a>
			</div>
  			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  <ul class="nav navbar-nav navbar-right">
			  <li><a href="index.php">Home</a></li>
			  <li><a href="news.php">News</a></li>
			  <li><a href="products.php">Products</a></li>
			  <li><a href="about.php">About</a></li>
			  <li><a href="contacts.php">Contacts</a></li>
			  <li><a href="cart.php"><?php echo $_SESSION['count'] ?>Items</a></li>
			  
			  </ul>
  			</div>
 		</div>
  	</nav>
    