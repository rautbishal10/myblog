<?php $title = "your page"; ?>
<?php session_start(); ?>
<?php include 'header.php' ?>
	<div class="row">
		<div class="col-md-9">
        <div class="jumbotron">
          <div class="container">
             <h1>Hello, user!</h1>
              <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
              <p><a class="btn btn-primary btn-lg" href="products.php" role="button">Browse Products &raquo;</a></p>
            </div>
          </div>
          <div class="">
          <a class="btn btn-success btn-lg" href="list_news.php">List News</a>
          <a class="btn btn-success btn-lg" href="add_news.php">Add News</a>
          <a class="btn btn-info btn-lg" href="#">List Products</a>
          <a class="btn btn-info btn-lg" href="#">Add Product</a>
        </div>
        </div>
		<div class="col-md-3">
		<?php include 'sidebar.php' ?>
		</div>
	</div>
<?php include 'footer.php' ?>