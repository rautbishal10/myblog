<?php $title = 'news';?>
<?php include 'header.php' ?>

  <div class="container">
  	<div class="row">

  		<div class="col-md-8">
            <!--including body news part-->
  			    <?php include 'bodynews.php' ?>

  			 
  		</div>

      <!--column for the sidebar at the side of the page-->
  		<div class="col-md-4">
            <!-- including side part of the page -->
              <div class="row">
                <?php include 'sidebar.php' ?>
              </div>

  			 
      </div>
    			
    </div>
  
  </div>
  
  <?php include 'footer.php'?>