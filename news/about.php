<?php $title = 'About';?>

<?php include 'header.php' ?>

<div class="container">

  	<div class="row">
        <div class="col-md-9">
    			<div class="head"><hr>
    				  <h2 style="color:brown" align="center">Up Coming Events</h2><hr>
    			</div>
        		<?php include 'slider.php' ?>
        </div>
        <div class="col-md-3">
            <?php include 'sidebar.php' ?>
        </div>    
          <?php include 'space.php' ?>
  	</div>
</div>

  <?php include 'footer.php'?>