 <?php $title = 'list_news';?>
<?php include 'header.php' ?>
<?php include 'db_connect.php' ?>
<div class="container">
 <div class="row">
        <div class="page-header"><h3>News List</h3></div>
        <div class="col-md-9">
            <div class="row news">
              <table class="table table-striped">
                <tr>
                  <th>S.No.</th>
                  <th>Title</th>
                  <th>Image</th>
                  <th>Body</th>
                  <th>Author</th>
                  <th>Published Date</th>
                  <th>Published?</th>
                  <th>Created On</th>
                  <th>Actions</th>
                </tr>
                <?php
                  $retval = mysql_query('SELECT *FROM news ');
                ?>
                <?php while($row = mysql_fetch_array($retval, MYSQL_ASSOC)):?>
                <tr>
                  <td><?php echo $row['id'];?></td>
                  <td><?php echo $row['title'];?></td>
                  <td><img src="<?php echo $row['image'];?>" /></td>
                  <td><?php echo substr($row['body'],0,100);?></td>
                  <td><?php echo $row['author'];?></td>
                  <td><?php echo $row['published_date'];?></td>
                  <td><?php echo $row['published'] ? 'Yes' : 'No';?></td>
                  <td><?php echo $row['created'];?></td>
                  <td>
                  <a href="admin_news_edit.php?id=<?php echo $row['id'];?>">Edit</a> <br>
                    <a  onclick="return confirm('Are you sure?')" href="admin_news_delete.php?id=<?php echo $row['id'];?>">Delete</a>
                  </td>
                </tr>
              <?php endwhile;?>
              </table>
            </div>
        </div>
        </div>
        </div>
<?php include 'footer.php' ?>