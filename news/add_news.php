<?php session_start();?>
<?php include 'db_connect.php';?>
<?php
  $error = '';

  if (!empty($_POST)){
  	if(isset($_FILES['image']) && $_FILES['image']['error'] == 0){
	    if(move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/news/'.$_FILES['image']['name'])){
			$image_path = 'uploads/news/'.$_FILES['image']['name'];
	      }
     }

     $title = $_POST['title'];
     $body = $_POST['editor1'];
     $aurthor = $_POST['aurthor'];
     $published_date = $_POST['published_date'];
     $published = $_POST['published'] == 'on' ? 1 : 0;

     $sql = "INSERT INTO news (title,body,aurthor,published_date,published,image) VALUES ('$title','$body','$aurthor','$published_date','$published','$image_path')";

     $retval = mysql_query( $sql, $conn );
     if(! $retval ) {
      $error = 'Error saving data : ' . mysql_error();
   	 }else{
   	 	header('Location:list_news.php?success&message=News Saved Successfully');
   	 }
   

   }
?>

 <?php $title = 'add_news';?>
<?php include 'header.php' ?>
<div class="container">
	<div class="row">
	<div class="page-header"><h3>Add News</h3></div>
		<div class="col-md-9">
			<script type="text/javascript" src="../js/ckeditor/ckeditor.js"></script>
			<div class="row news">
	              <form class="form-horizontal" method="POST" enctype="multipart/form-data">
			              <div class="form-group">
			                <label for="title" class="col-sm-2 control-label">Title</label>
			                <div class="col-sm-10">
			                  <input type="text" name="title" class="form-control" id="title" required>
			                </div>
			              </div>
			              <div class="form-group">
			                <label for="title" class="col-sm-2 control-label">Aurthor</label>
			                <div class="col-sm-10">
			                  <input type="text" name="aurthor" class="form-control" id="aurthor" required>
			                </div>
			              </div>
			              <div class="form-group">
			                <label for="body" class="col-sm-2 control-label">Body</label>
			                <div class="col-sm-10">
			                  <textarea name="editor1" id="editor1" class="form-control" id="title" required></textarea>
			                </div>
			              </div>
			              <div class="form-group">
			                <label for="published_date" class="col-sm-2 control-label">Published Date</label>
			                <div class="col-sm-10">
			                  <input type="date" name="published_date" class="form-control" id="published_date" required>
			                </div>
			              </div>
			              <div class="form-group">
			                <label for="published" class="col-sm-2 control-label">Published</label>
			                <div class="col-sm-10">
			                  <input type="checkbox" name="published" class="form-control" id="published">
			                </div>
			              </div>
			              <div class="form-group">
			                <label for="image" class="col-sm-2 control-label">Image</label>
			                <div class="col-sm-10">
			                  <input type="file" name="image" class="form-control" id="image">
			                </div>
			              </div>
			              <div class="form-group">
			                <div class="col-sm-offset-2 col-sm-10">
			                  <button type="submit" name = "submit" class="btn btn-default">Submit</button>
			                </div>
			                </div>
			       </form>

	            </div>
	            
        </div>
        		<div class="col-md-3">
					<?php include 'sidebar.php' ?>
				</div>
    </div>
</div>
<script type="text/javascript">
   CKEDITOR.replace('editor1');
</script>
<?php include 'footer.php' ?>