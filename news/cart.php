 <?php $title = "cart"; ?>
 <?php session_start(); ?>
 <?php include 'header.php' ?>
 <div class="container">
 <div class="row">
 <div class="col-md-9">
 <div class="page-header"><h3>Cart</h3></div>
        <div class="col-md-12">
            <div class="row">
            
                          <table class="table">
                <tr>

                  <th>S.No.</th>
                  <th>Name</th>
                  <th>Price</th>
                  <th>Actions</th>
                </tr>
               <?php $i = 0 ; ?> 
               <?php foreach($_SESSION['cart'] as $key => $cart):?>
                  <tr>
                    <td><?php echo ++$i ?></td>
                    <td><?php echo $cart['name'] ?></td>
                    <td><?php echo $cart['price'] ?></td>
                    <td><a class="btn btn-xs btn-warning" href="removefromcart.php?key=<?php echo $key; ?>">Remove</a></td>
                  </tr> <?php endforeach ?>
                   
                                </table>
                          </div>
        </div>
        </div>
        <div class="col-md-3">
        <?php include 'sidebar.php' ?>
        </div>
</div>
</div>
<?php include 'footer.php' ?>