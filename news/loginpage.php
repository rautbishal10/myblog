<?php $title = 'loginpage'; ?>
<?php include 'header.php' ?>
    <div class="row">
    <div class="col-md-6 col-md-offset-3">
	  <!--Bootstrap form table -->
          <form class="form-horizontal" method="POST" action="user.php">
              <div class="form-group">
                          <h3>&emsp; Please Login</h3><hr><br>
                      <label class="control-label col-sm-2" for="email">User Email:</label>
                  <div class="col-sm-10">
                      <input type="text" class="form-control" name='email' placeholder="enter user name" required >
                  </div>
              </div>
              <div class="form-group">
                      <label class="control-label col-sm-2" for="password">Password:</label>
                  <div class="col-sm-10">
                      <input type="password" class="form-control" name="password" placeholder="enter password" required >
                  </div>
              </div>
              <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                      <label><input type="checkbox"> Remember me</label>
                    </div>
                  </div>
              </div>
              <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
              </div>
          </form>
    </div>
    <div class="col-md-3">
    <?php include 'sidebar.php' ?>
    </div>
    </div>
<?php include 'footer.php' ?>
